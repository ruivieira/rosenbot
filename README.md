# rosenbot

A musical bot that listens.

## running

Suitable only for testing at the moment.
Install dependencies using

```bash
$ pip install -r requirements.txt
```

and then

```bash
$ python app.py
```

## mutations

At the moment using a pseudo rejection-sampling algorithm.

<img src="docs/diagram.png"  width="320">

## extending

You can create your own elements by extending the `Mutable` class.
The main methods to implement are `mutate`, which defines how the element will mutate and `sound` which _has_ to return
a `dsp.buffer`.

## acknowledgements

Samples from [Freesound](https://freesound.org/), licenced under [CC 3.0](https://creativecommons.org/licenses/by/3.0/).