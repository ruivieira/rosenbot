from distutils.core import setup

setup(
    name='rosenbot',
    packages=['rosenbot'],
    version='0.0.3',
    description='A musical bot framework',
    author='Rui Vieira',
    author_email='ruidevieira@googlemail.com',
    url='https://gitlab.com/ruivieira/rosenbot',
    download_url='https://gitlab.com/ruivieira/rosenbot/-/archive/0.0.3/rosenbot-0.0.3.tar.gz',
    keywords=['bot', 'music', 'sound', 'framework', 'generative'],
    classifiers=[],
)